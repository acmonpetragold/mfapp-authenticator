/**
 * @fileoverview includes a route object with
 * a `children` attribute that allows a router-view
 * component to be renderized on it.
 */

export default {
  path: '/authenticator',
  children: [
    {
      path: '',
      name: 'aut.realm.index',
      meta: {
        authorization: true,
        title: 'Projeto',
        permission: 'realm:list'
      },
      component: () => import('../views/realm/Index')
    },
    {
      path: 'cadastrar',
      name: 'aut.realm.create',
      meta: {
        authorization: true,
        title: 'Cadastrar',
        permission: 'realm:create'
      },
      component: () => import('../views/realm/Form')
    },
    {
      path: 'editar/:id',
      name: 'aut.realm.update',
      meta: {
        authorization: true,
        title: 'Editar',
        permission: 'realm:update'
      },
      component: () => import('../views/realm/Form')
    }
  ]
}
