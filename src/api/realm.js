import http from '@/lib/http'
import oops from '@/lib/oops'

const RESOURCE = '/realms'

export default {
  /**
   * Get realms list data
   */
  async list (filter) {
    try {
      return await http.get(`${RESOURCE}`, {
        params: filter
      })
        .then((res) => res.data)
    }
    catch (err) {
      console.error(err)
      throw new oops.Wrap(err).throwedBy('axios')
    }
  },

  async get (id) {
    try {
      return await http.get(`${RESOURCE}/${id}`)
        .then((res) => res.data)
    }
    catch (err) {
      throw new oops.Wrap(err).throwedBy('axios')
    }
  },

  async save (realm) {
    try {
      let method = 'post'
      if (realm._id) method = 'put'

      return await http[method](`${RESOURCE}/${realm._id || ''}`, realm)
        .then((res) => res.data)
    }
    catch (err) {
      throw new oops.Wrap(err).throwedBy('axios')
    }
  },

  /**
   * Delete product by id
   */
  async delete (id) {
    try {
      await http.delete(`${RESOURCE}/${id}`)
    }
    catch (err) {
      console.error(err.message)
      throw new oops.Wrap(err).throwedBy('axios')
    }
  },

}
