export default [
  {
    icon: 'gpi-broken-car',
    label: 'Projeto',
    to: { name: 'aut.realm.index' },
    permissions: 'realm.list'
  }
]
