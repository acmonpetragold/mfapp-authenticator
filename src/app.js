import routes from './routes'
import modules from './store'
import links from './layout/links'
import { version } from '../package.json'

export default {
  namespace: 'authenticator',
  version,
  routes,
  modules,
  layout: {
    name: 'Authenticator',
    links
  }
}
