import { ui } from 'vuetify-petragold'
import repository from '../api/realm'

const state = {
  loading: false,
  list: [],
  current: {}
}

const actions = {
  async list ({ commit }, filter = '') {
    try {
      state.loading = true
      const items = await repository.list(filter);

      commit('SET_LIST', items)
    }
    catch (err) {
      console.error(err.message)
    }
    finally {
      state.loading = false
    }
  },

  /**
  * Get user app
  * @param {Object} vuex
  * @param {String} app
  */
  async get ({ commit, state }, id) {
    state.current = {}
    state.loading = true

    const current = await repository.get(id)
      .catch((err) => {
        ui.alert(`Não foi possível encontrar este cliente. ${err.message}`)
      })

    if (current) commit('SET_STATE', { current })

    state.loading = false
    return current
  },

  async save (_vuex, realm) {
    try {
      state.loading = true
      await repository.save(realm)

      ui.alert('Ação realizada com sucesso.')
    }
    catch ({ message }) {
      ui.alert(message)
      throw new Error(message)
    }
    finally {
      state.loading = false
    }
  },

  async delete ({ dispatch }, id) {
    try {
      state.loading = true
      await repository.delete(id)
      dispatch('list')
    }
    catch (err) {
      ui.alert(err.message)
      throw new Error(err.message)
    }
    finally {
      state.loading = false
    }
  }
}

const mutations = {
  /**
   * Change realms list state
   * @param {VuexState} state
   * @param {Array} realms
   */
  SET_LIST (state, realms) {
    state.list = realms
  },

  SET_STATE (state, value) {
    Object
      .entries(value)
      .forEach(([k, v]) => {
        state[k] = v
      })
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
