module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: 'off',
    'comma-dangle': 'off',
    'no-prototype-builtins': 'off',
    'no-shadow': 'off',
    'import/no-cycle': 'off',
    'space-before-function-paren': ['error', 'always'],
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'brace-style': ['error', 'stroustrup'],
    'no-param-reassign': ['error', { props: false }],
    'global-require': 'off',
    'no-underscore-dangle': ['error', { allow: ['_id'] }]
  },
}
